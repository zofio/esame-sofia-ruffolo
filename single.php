<?php

get_header(); ?>
<main class="main">

  <section class="all_post archive_post">
      <div class="container">

        <div class="single_catagory_post post_2">
            <div class="category_post_img">
                <?php the_post_thumbnail('full'); ?>
                 <p><?php the_category(); ?></p>
            </div>
        </div>
          <div class="row">
              <div class="col-lg-8">
                  <div class="row">

                    <?php while ( have_posts() ) : the_post(); ?>


                              <div class="post_text_1 pr_30">
                                  <p><span> <?php the_author(); ?></span><?php the_time('F, j, Y'); ?></p>
                                  <a href="<?php the_permalink(); ?>">
                                      <h3><?php the_title(); ?></h3>

                                  </a>
                                  <p><?php the_content();?></p>
                                  <?php echo get_post_meta($post->ID, 'name', true); ?>
                                  <?php wp_link_pages( array(
                										'before' => '<div>' . __( 'Pages:', 'esame' ),
                										'after'  => '</div>',)); ?>
                              </div>


                      <?php endwhile; ?>


                  </div>
              </div>
              <div class="col-lg-4">
                  <div class="sidebar_widget">


                      <div class="single_catagory_item category">
                          <?php get_sidebar(); ?>
                      </div>

                  </div>
              </div>
              </div>
          </div>
      </div>
  </section>






</main>
<?php get_footer(); ?>
