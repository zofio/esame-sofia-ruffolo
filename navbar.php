<?php
if ( function_exists( 'wp_nav_menu' ) && has_nav_menu( 'main-menu' ) ) {
  wp_nav_menu( array(
    'theme_location'  => 'main-menu',
    'menu_id' => 'navbarSupportedContent',
    'menu_class' => 'navbar-nav'

  ) );
}
