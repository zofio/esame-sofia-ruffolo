<?php

$args = array(
	'posts_per_page' => 1, // how many posts.
);

$featured_post = new WP_Query( $args );

if ( $featured_post->have_posts() ) {
	while ( $featured_post->have_posts() ) {
		$featured_post->the_post();
		// Save Post ID.
		$featured_id = $post->ID;
		// Codice HTML per mostrare il Post.
		?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="banner_post_1 banner_post_bg_1">
                    <div class="banner_post_iner text-center">
                        <a href="category.html"><h5> <?php the_category(); ?></h5></a>
                        <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?> </h2></a>
                        <p><span> <?php the_author(); ?></span>  <?php the_time('F,j,Y'); ?></p>
                    </div>
                </div>
                <div class="banner_post_2 banner_post_bg_2">
                    <div class="banner_post_iner">
                        <a href="category.html"><h5> <?php the_category(); ?></h5></a>
                        <a href="<?php the_permalink(); ?>"><h2><?php the_title(); ?></h2></a>
                        <p><span> <?php the_author(); ?></span>  <?php the_time('F,j,Y'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>


		<?php
	} // End loop.
}
