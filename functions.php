<?php
  require_once dirname( __FILE__ ) . '/shortcodes.php';
  function esame_enqueue_style_and_script(){
    wp_enqueue_style('stylesheet', get_stylesheet_uri());
    wp_enqueue_style('boostrap', get_template_directory_uri() . '/css/bootstrap.min.css');

    wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css');

    wp_enqueue_style('carousel', get_template_directory_uri() . '/css/owl.carousel.min.css');

    wp_enqueue_style('themify', get_template_directory_uri() . '/css/themify-icons.css');

    wp_enqueue_style('liner_icon', get_template_directory_uri() . '/css/liner_icon.css');

    wp_enqueue_style('search', get_template_directory_uri() . '/css/search.css');

    wp_enqueue_style('style', get_template_directory_uri() . '/css/style.css');


    //java_script
    wp_enqueue_style('jquery', get_template_directory_uri() . '/js/jquery-1.12.1.min.js',true, mt_rand(0,4));

    wp_enqueue_style('popper', get_template_directory_uri() . '/js/popper.min.js', array('jquery'),true, mt_rand(0,4));

    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'),true, mt_rand(0,4));

    wp_enqueue_style('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'),true, mt_rand(0,4));


  }
  add_action ('wp_enqueue_scripts','esame_enqueue_style_and_script');

  function esame_init() {
    register_taxonomy(
  		'portfolio_categories',
  		'portfolio',
  		array(
  			'label'             => __( 'Portfolio Categories', 'esame' ),
  			'public'            => true,
  			'show_admin_column' => true,
  			'rewrite'           => array( 'slug' => 'portfolio-categories' ),
  			'hierarchical'      => true,
  		)
  	);
  	// Register dynamic sidebar.
  	register_sidebar( array(
  		'name' => 'Right Sidebar',
  		'id'   => 'right-sidebar',
      'description'   => '',
      'before_widget' => '<div id="%1$s" class="widget %2$s">',
      'after_widget'  => "</div>\n",
      'before_title'  => '<h3 class="widgettitle">',
      'after_title'   => "</h3>\n"
  	) );
  }

  add_action( 'init', 'esame_init' );

  function esame_after_setup_theme(){
    load_theme_textdomain('esame');
   // Let WordPress take care of the `<title>` tag.
   add_theme_support( 'title-tag' );
   // Add support for Featured Images.
   add_theme_support( 'post-thumbnails' );
   // Default image size.
   set_post_thumbnail_size( 160, 160, true );

   add_theme_support( 'menus' );
   // Add custom image size.
   add_image_size( 'custom-image-size', 700, 420, true );

  

    register_nav_menus( array(
  		'main-menu'   => 'Main Menu',
  		'footer-menu' => 'Footer Menu',
  		'social-link' => 'Social Menu',
  	));
    }
  add_action( 'after_setup_theme', 'esame_after_setup_theme' );

  function esame_register_portfolio_post_type() {

  	//Register `Portfolio` Custom Post Type.
  	register_post_type( 'portfolio', array(
  		'labels'      => array(
  			'name'          => __( 'Portfolio', 'esame' ),
  			'singular_name' => __( 'Portfolio Item', 'esame' ),
  		),
  		'public'      => true,
  		'menu_icon'   => 'dashicons-format-image',
  		'supports'    => array(
  			'title',
  			'editor',
  			'thumbnail',
  			'excerpt',
  			'custom-fields',
  			'author',
  			'revisions',
  		),
  		'taxonomies'  => array(
  			'categories',
  			'post_tag',
  			'category'
  		),
  		'has_archive' => true,
  		'rewrite'     => array( 'slug' => 'portfolio' ),
  	) );
  }

  function esame_flush_rewrite_rules() {
  	esame_register_portfolio_post_type();
  	flush_rewrite_rules();
  }


  add_action( 'init', 'esame_register_portfolio_post_type' );
  add_action( 'after_switch_theme', 'esame_flush_rewrite_rules' );




 ?>
