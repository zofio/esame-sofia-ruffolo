<?php get_header(); ?>

    <!--::breadcrumb part start::-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <h2><?php the_archive_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--::breadcrumb part start::-->

    <!-- feature_post start-->
    <section class="all_post archive_post">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="row">
                      <?php if ( have_posts() ) : ?>
                      <?php while ( have_posts() ) : the_post(); ?>
                        <div class="col-lg-6">
                        <?php  get_template_part('content/content', 'excerpt') ?>
                        </div>
                        <?php endwhile; ?>
                        <?php endif; ?>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="sidebar_widget">


                        <div class="single_catagory_item category">
                            <?php get_sidebar(); ?>
                        </div>

                    </div>
                </div>
                </div>
            </div>
        </div>
    </section>
    <?php the_posts_pagination(
                array(
                  'prev_text' => sprintf( '<span class="previous-posts">%s</span>',
                    __( 'Previous', 'esame' )
                  ),

                  'next_text' => sprintf( '<span class="next-posts">%s</span>',
                    __( 'Next', 'esame' )
                  ),
                )
              ); ?>
    <!-- feature_post end-->
<?php get_footer(); ?>
