
    <div class="single_catagory_post post_2">
        <div class="category_post_img">
            <?php the_post_thumbnail('full'); ?>
             <p><?php the_category(); ?></p>
        </div>
        <div class="post_text_1 pr_30">
            <p><span> <?php the_author(); ?></span><?php the_time('F, j, Y'); ?></p>
            <a href="<?php the_permalink(); ?>">
                <h3><?php the_title(); ?></h3>
            </a>
            <?php the_excerpt(); ?>

        </div>
    </div>
