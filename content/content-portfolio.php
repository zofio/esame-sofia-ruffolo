
    <div class="single_catagory_post post_2">
        <div class="category_post_img">
            <?php the_post_thumbnail('full'); ?>
            <?php if ( get_the_terms( get_the_ID(), 'portfolio_categories' ) ) {
              the_terms( get_the_ID(), 'portfolio_categories','<ul class="portfolio-terms"><li>', '</li><li>', '</li></ul>' );}?>
        </div>
        <div class="post_text_1 pr_30">
            <p><span> <?php the_author(); ?></span><?php the_time('F, j, Y'); ?></p>
            <a href="<?php the_permalink(); ?>">
                <h3><?php the_title(); ?></h3>
            </a>
            <?php
          // Display excerpt only in Archive.
  		      if ( ! is_front_page() || ! is_home() ) {
  			         the_excerpt();
  		           }
  		        ?>

        </div>
    </div>
