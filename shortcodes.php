<?php

add_shortcode('esame_blockquote', 'esame_add_blockquote_shortcode');

function esame_add_blockquote_shortcode($atts, $content = null){
  $atts = shortcode_atts( array(
	'text' => 'Some text',
  ), $atts );
  ob_start();?>
  <blockquote> <?php echo $atts['text']; ?> </blockquote>
	<?php return ob_get_clean();

}
