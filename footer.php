    <footer class="footer-area">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-sm-6">
                    <div class="single-footer-widget footer_1">
                        <h4><?php _e('About me', 'esame'); ?></h4>
                        <p><?php _e('Do you want to be even more successful? Learn to love learning and growth.
                            The more effort you put into improving your skills, the bigger the payoff
                            you will get. Realize that things will be hard at first, but the rewards
                            will be worth it.', 'esame'); ?></p>

                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-6">
                    <div class="single-footer-widget footer_2">
                        <h4><?php _e('Contact us', 'esame'); ?></h4>
                        <div class="contact_info">
                            <span class="ti-home"></span>
                            <h5><?php _e('Los angeles', 'esame'); ?></h5>
                            <p><?php _e('659, Rocky beach bullevard, santa monica, Rocky beach, USA.', 'esame'); ?></p>

                        </div>
                        <div class="contact_info">
                            <span class="ti-headphone-alt"></span>
                            <h5>+44 6532 986 652</h5>
                            <p>Mon to Fri 9am to 6 pm.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-sm-10">
                    <div class="single-footer-widget footer_3">
                        <h4><?php _e('Newsletter', 'esame'); ?></h4>
                        <p><?php _e('Stay updated with our latest trends The more effort you put into
                        improving your skills, the bigger the payoff you will get realize that things.', 'esame'); ?></p>

                        <form action="#">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control" placeholder='Enter email address'
                                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter email address'">
                                    <div class="input-group-append">
                                        <button class="btn" type="button"><span class="lnr lnr-arrow-right"></span></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="copyright_part_text text-center">
                        <p class="footer-text m-0"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="ti-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
  <?php wp_footer(); ?>
</body>

</html>
